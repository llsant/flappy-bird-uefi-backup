Intro
=====
This repo is intended as a backup of https://github.com/hymen81/UEFI-Game-FlappyBirdy

Some other files are included for convenience. The "disk" directory contains the
default EFI loader, which is in fact the compiled Flappy Bird game.

The bios/ directory contains OVMF (UEFI) firmware for QEMU, from EDK2. I extracted
the files from https://www.kraxel.org/repos/jenkins/edk2/edk2.git-ovmf-x64-0-20191118.1338.g4c0f6e349d.noarch.rpm
though maybe there were better places

How to run
==========

qemu-system-x86_64 -bios bios/OVMF-pure-efi.fd -hda fat:rw:disk

Screenshot
========== 
![Flappy Bird](screenshot.png)
